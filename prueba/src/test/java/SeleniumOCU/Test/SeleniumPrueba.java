package SeleniumOCU.Test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SeleniumPrueba {
	// Definimos elementos
	private WebDriver driver;

	@FindBy(linkText = "Calcular hipoteca")
	WebElement linkCalcularHipoteca;

	@FindBy(id = "financial_income")
	WebElement ingresosMensuales;

	@FindBy(id = "living_place-price-ammount")
	WebElement precioVivienda;

	@FindBy(id = "id_acc_provincias-button")
	WebElement bltSeleccionarProvincia;

	@FindBy(id = "ui-id-52")
	WebElement opcionSevilla;

	@FindBy(css = ".mod_buttonsSimulador")
	WebElement contendorCalcular;

	@FindBy(xpath = "//div[@class='content-infoBlock'][2]//button")
	WebElement btnCalcular;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\driverSelenium\\chrome\\chromedriver.exe");
		// Inicializamos el driver
		driver = new ChromeDriver();
		// Abrimos el navegador y maximizamos la pantalla
		driver.get("https://www.bankia.es/es/particulares");
		driver.manage().window().maximize();
		// Inicializamos los elementos definidos anteriormente
		PageFactory.initElements(driver, this);
	}

	@Test
	public void pruebaBankia() {

		// Email profe: mcyagues@gfi.es
		/**
		 * WebElement ingresosMensuales = driver.findElement(By.id("financial_income"));
		 * ingresosMensuales.sendKeys("5000"); WebElement precioVivienda =
		 * driver.findElement(By.id("living_place-price-ammount"));
		 * precioVivienda.clear(); precioVivienda.sendKeys("100000");
		 * driver.findElement(By.id("id_acc_provincias-button")).click();
		 * driver.findElement(By.id("ui-id-53")).click(); WebElement btnCalcular =
		 * driver.findElement(By.xpath("//button[@onclick=\"
		 * onClickEnlaceInteres('simulador de hipotecas nuevo:ver simulacion','');
		 * return validacionPrimeraVista()\"]")); btnCalcular.click();
		 */
		try {
			linkCalcularHipoteca.click();
			ingresosMensuales.sendKeys("5000");
			precioVivienda.clear();
			precioVivienda.sendKeys("100000");
			bltSeleccionarProvincia.click();
			opcionSevilla.click();
			btnCalcular.click();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}

	}

	@After
	public void close() throws Exception {
		try {
			Thread.sleep(5000);
			driver.close();
		} catch (InterruptedException e) {
			// TODO: Auto-generated catch block
			e.printStackTrace();
		}
	}
}
